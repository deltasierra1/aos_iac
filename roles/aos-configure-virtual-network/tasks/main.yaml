---
# ----------------------------------------------
# Load intent and process it, to prepare API request
# ----------------------------------------------
  - name: "{{ file_name }} - Load intent file" 
    include_vars:
      file: "{{ item }}"
      name: vn_intent_object
    check_mode: no


  - name: "{{ file_name }} - Create empty lists"
    set_fact:
      node_name_system_id: []
      bound_to: []
      # virtual_network_id: []
    check_mode: no


  - name: "{{ file_name }} - Find \"system_id\" per node" 
    set_fact:
      node_name_system_id: "{{ (node_name_system_id|default([])) + bound_to_all | json_query(query) | list }}"
    vars:
      query: "[?label=='{{ selected_leaf.node_name }}'].{node_name: label, system_id: system_id}"
    loop: "{{ vn_intent_object.assigned_to }}"
    loop_control:
      loop_var: selected_leaf
      # extended: yes
    check_mode: no

# ----------------------------------------------
# Note: task output can be long
# ----------------------------------------------
  - name: "{{ file_name }} - Create \"bound_to\" structure"
    set_fact:
      bound_to: "{{ bound_to|default([]) + [{ 'system_id': node[1].system_id, 'vlan_id': node[0].vlan_id }] }}"
    when: "node[0].node_name == node[1].node_name"
    loop: "{{ query('nested', vn_intent_object.assigned_to, node_name_system_id) }}"
    loop_control:
      loop_var: node
      label: bound_to
      # extended: yes
    check_mode: no


  - name: "{{ file_name }} - Find \"security_zone_id\""
    set_fact:
      security_zone_id: "{{ security_zones | json_query(query) | first }}"
    vars:
      query: "json.items.*.{label: label, id:id}[?label=='\
        {{ vn_intent_object.security_zone }}'].id"
    ignore_errors: "{{ ansible_check_mode }}"


  - name: "{{ file_name }} - Add \"bound_to\" and \"security_zone_id\""
    set_fact:
      object_name: "{{ vn_intent_object | combine(keys_to_add) }}"
    vars:
      keys_to_add:
        bound_to: "{{ bound_to }}"
        security_zone_id: "{{ security_zone_id }}"
    ignore_errors: "{{ ansible_check_mode }}"


  - name: "{{ file_name }} - Generate Request body"
    set_fact:
      request_body: "{{ object_name | json_query(keys_to_keep) }}"
    vars:
      keys_to_keep: '{
        label: label
        security_zone_id: security_zone_id
        vn_type: vn_type
        ipv4_enabled: ipv4_enabled
        ipv4_subnet: ipv4_subnet
        virtual_gateway_ipv4: virtual_gateway_ipv4
        bound_to: bound_to
        default_endpoint_tag_types: link_labels
      }'
    ignore_errors: "{{ ansible_check_mode }}"

# ----------------------------------------------
# Check presence of the SZ
#   If  yes  Then  use PUT   (Update)
#   else           use POST  (Create)
# ----------------------------------------------
  - name: "{{ file_name }} - Get \"virtual_network_id\" if exists"
    set_fact:
      virtual_network_id: "{{ current_virtual_networks | json_query(query_vn_id) | first }}"
    when: vn_intent_object.label in current_virtual_networks | json_query(query_vn_labels)
    vars:
      query_vn_id: "json.virtual_networks.*.{label: label, id:id}[?label==\
        '{{ vn_intent_object.label }}'].id"
      query_vn_labels: "json.virtual_networks.*.label"
    ignore_errors: "{{ ansible_check_mode }}"


# ----------------------------------------------
# Push request to AOS
#   Opertion = Update -> method = PUT
# ----------------------------------------------
  - name: "{{ file_name }} - Update Virtual Network (PUT API call)"
    uri:
      url: "{{ aos_api_url }}/blueprints/{{ blueprint_id }}/virtual-networks/\
        {{ virtual_network_id }}"
      method: PUT
      status_code: 204
      validate_certs: no
      headers: "{{ aos_api_headers }}"
      body: "{{ request_body }}"
      body_format: json
    when: 
      - vn_intent_object.label in current_virtual_networks | json_query(query_vn_labels)
      - virtual_network_id is defined
    vars:
      query_vn_labels: "json.virtual_networks.*.label"
    register: virtual_network
    ignore_errors: yes
    changed_when:
      - virtual_network.status == 204


# ----------------------------------------------
# Push request to AOS
#   Opertion = Create -> method = POST
# ----------------------------------------------
  - name: "{{ file_name }} - Create Virtual Network (POST API call)"
    uri:
      url: "{{ aos_api_url }}/blueprints/{{ blueprint_id }}/virtual-networks"
      method: POST
      status_code: 201
      validate_certs: no
      headers: "{{ aos_api_headers }}"
      body: "{{ request_body }}"
      body_format: json
    when: vn_intent_object.label not in current_virtual_networks | json_query(vn_labels)
    vars:
      vn_labels: "json.virtual_networks.*.label"
    register: virtual_network
    ignore_errors: yes
    changed_when:
      - virtual_network.status == 201
